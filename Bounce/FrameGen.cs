﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Bounce
{
    class FrameGen
    {
        public Bitmap Render(Game game)
        {
            Bitmap bmp = new Bitmap((int)game.BoardWidth, (int)game.BoardHeight);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.SkyBlue);

            Point ballP = new Point((int)game.GameBall.Position.X - (int)game.GameBall.Radius, (int)game.GameBall.Position.Y - (int)game.GameBall.Radius);
            g.DrawImage(Resource1.ball, ballP);

            Point paddleP = new Point((int)game.Player.Position.X, (int)game.Player.Position.Y);
            g.DrawImage(Resource1.paddle, paddleP);

            //g.DrawEllipse(Pens.Black, game.GameBall.Position.X - game.GameBall.Radius, game.GameBall.Position.Y - game.GameBall.Radius, game.GameBall.Radius * 2, game.GameBall.Radius * 2);
            //g.DrawRectangle(Pens.Black, game.Player.Position.X, game.Player.Position.Y, game.Player.Width, game.Player.Height);

            return bmp;
        }
    }
}
