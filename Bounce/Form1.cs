﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bounce
{
    public partial class Form1 : Form
    {
        private Bitmap bmp;
        private Graphics g;
        private Game game;
        private FrameGen fg;

        public Form1()
        {
            InitializeComponent();
        }

        private DateTime lastFrame;
        private void Form1_Load(object sender, EventArgs e)
        {
            this.label7.Location = new Point(this.Width / 2, this.Height - 300);
            this.label8.Location = new Point(this.Width / 2, this.Height - 400);
            this.label9.Location = new Point(this.Width / 2, this.Height - 500);
            this.nearVel = 0;
            this.midVel = 0;
            this.farVel = 0;

            Cursor.Hide();
            game = new Game(this.Width, this.Height);
            bmp = new Bitmap(this.Width, this.Height);
            g = Graphics.FromImage(bmp);
            fg = new FrameGen();
            pictureBox1.Image = bmp;
            g.Clear(Color.Black);
            lastFrame = DateTime.Now;
            drawGame();
        }

        // take out parallax
        private int nearVel { get; set; }
        private int midVel { get; set; }
        private int farVel { get; set; }
        private void parallax()
        {
            nearVel = game.bounceCount * 4;
            midVel = game.bounceCount * 2;
            farVel = game.bounceCount * 1;

            if(label7.Location.X < -50)
                this.label7.Location = new Point(this.Width - nearVel, this.Height - 300);
            else
            this.label7.Location = new Point(this.label7.Location.X - nearVel, this.Height - 300);

            if (label8.Location.X < -50)
                this.label8.Location = new Point(this.Width - midVel, this.Height - 300);
            else
                this.label8.Location = new Point(this.label8.Location.X - midVel, this.Height - 400);

            if (label9.Location.X < -50)
                this.label9.Location = new Point(this.Width - farVel, this.Height - 300);
            else
                this.label9.Location = new Point(this.label9.Location.X - farVel, this.Height - 500);
        }

        private bool active { get; set; }
        private void drawGame()
        {
            active = true;
            while(active)
            {
                this.label1.Text = "Elevation = " + game.elevation.ToString();
                this.label2.Text = "Bounces = " + game.bounceCount.ToString();
                this.label3.Text = "Position = X:" + game.GameBall.Position.X.ToString() + " Y: " + game.GameBall.Position.Y.ToString();
                this.label4.Text = "Velocity = X:" + game.GameBall.Velocity.X.ToString() + " Y: " + game.GameBall.Velocity.Y.ToString();
                this.label5.Text = "Acceleration = X:" + game.GameBall.Acceleration.X.ToString() + " Y: " + game.GameBall.Acceleration.Y.ToString();

                

                float secondsElapsed = (float)(DateTime.Now - lastFrame).TotalSeconds;
                lastFrame = DateTime.Now;
                game.AdvanceTime(secondsElapsed);
                parallax();
                //Image image = Image.FromFile(@"C:\documents\cmu\bounce\ball.png");
                //Point p = new Point((int)game.GameBall.Position.X - (int)game.GameBall.Radius, (int)game.GameBall.Position.Y - (int)game.GameBall.Radius);
                //g.DrawImage(image, p);
                g.DrawEllipse(Pens.White, game.GameBall.Position.X-game.GameBall.Radius, game.GameBall.Position.Y - game.GameBall.Radius, game.GameBall.Radius *2, game.GameBall.Radius*2);
                g.DrawRectangle(Pens.White, game.Player.Position.X, game.Player.Position.Y, game.Player.Width, game.Player.Height);

                pictureBox1.Image = fg.Render(game);
                pictureBox1.Refresh();
                this.Text = game.elevation.ToString();
                Application.DoEvents();
            }
        }


        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {   //get paddle to stop when a paddle edge hits a screen edge
            game.Player.Position.X = (e.X - (game.Player.Width / 2));
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    game.IsActive = !game.IsActive;
                    label6.Visible = !label6.Visible;
                    break; //pause everything, restrict paddle movement as well
                case Keys.Enter:
                    this.FormBorderStyle = FormBorderStyle.Sizable;
                    active = false;
                    break;
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    game.MouseState = 1;
                    break;
                case MouseButtons.Right:
                    game.MouseState = 2;
                    break;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            game.MouseState = 0;
        }

    }
}
