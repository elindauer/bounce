﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bounce
{
    class Ball
    {
        public float Radius { get; set; }
        public Vector Velocity { get; set; }
        public Vector Position { get; set; }
        public Vector Acceleration { get; set; }

        public Ball(float xVel, float yVel)
        {
            this.Velocity = new Vector();
            this.Position = new Vector();
            this.Acceleration = new Vector();
            this.Velocity.X = xVel;
            this.Velocity.Y = yVel;
        }

        public void AdvanceTime(float timeStep)
        {
            this.Position.X += this.Velocity.X * timeStep;
            this.Position.Y += this.Velocity.Y * timeStep;
            this.Velocity.Y += this.Acceleration.Y;
            this.Velocity.X += this.Acceleration.X;
        }

        public void ballSound()
        {
            //play an audio clip for bouncing ball
        }

        public Boolean hitWall(float width)
        {
            if ((this.Position.X - this.Radius < 0)||(this.Position.X + this.Radius > width))
                return true;
            else
                return false;
        }

        public void bounceX(float width)
        {   
            this.Velocity.X *= -1;
            if(this.Position.X-this.Radius < 10)
                this.Position.X = this.Radius;
            else
                this.Position.X = (width - this.Radius);
            ballSound();
        }

        public void bounceY()
        {
            this.Velocity.Y *= -1;

        }

        public void hitPaddle()
        {
            this.bounceY();
            ballSound(); //make a custom sound for hitting paddle?
        }

        public Boolean top()
        {
            if (this.Position.Y < 0) //cut ball in half
                return true;
            else
                return false;
        }

        public Boolean bottom(float height)
        {
            if (this.Position.Y+this.Radius+40 > height) //cut ball in half
                return true;
            else
                return false;
        }

        public Boolean overPaddle(Paddle PAD, float height)
        {
            if ((this.Position.X > PAD.Position.X) && (this.Position.X < PAD.Position.X + PAD.Width))
                return true;
            else
                return false;
        }
    }
}
