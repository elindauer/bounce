﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bounce
{
    class Paddle
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public Vector Position { get; set; }

        public Paddle()
        {
            this.Position = new Vector();
        }

        public void Move(float amount)
        {
            this.Position.X += amount;
        }
    }
}
