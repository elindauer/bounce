﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bounce
{
    class Game
    {
        const int GRAVITY = 75;
        public bool IsActive { get; set; }
        public float BoardWidth { get; set; }
        public float BoardHeight { get; set; }
        public Ball GameBall { get; set; }
        public Paddle Player { get; set; }

        public Game(float width, float height)
        {
            this.IsActive = true;
            ResetGame(width, height);

        }

        public void ResetGame(float width, float height)
        {
            this.gravity = GRAVITY;
            this.BoardWidth = width;
            this.BoardHeight = height;
            this.bounceCount = 0;
            this.elevation = 0;
            this.MouseState = 0;
            Player = new Paddle();
            GameBall = new Ball(400,-1000);
            GenerateRatio();
            
        }

        public void GenerateRatio()
        {
            this.GameBall.Radius = 50;//this.BoardHeight / 40;
            this.GameBall.Position.X = (this.BoardWidth / 2) - (this.GameBall.Radius);
            this.GameBall.Position.Y = (this.BoardHeight / 2) - (this.GameBall.Radius);
            this.Player.Width = 265;//this.BoardWidth / 10;
            this.Player.Height = 30;//this.BoardHeight / 30;
            this.Player.Position.X = (this.BoardWidth / 2) - (Player.Width / 2);
            this.Player.Position.Y = this.BoardHeight - this.Player.Height;
        }

        public float GetRandom(int low, int high)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            return r.Next(low, high + 1);
        }

        private float gravity { get; set; }
        public int bounceCount { get; set; }
        public int elevation { get; set; }
        public int MouseState { get; set; }

        public void AdvanceTime(float timeStep)
        {
            if (this.IsActive)
            {
                GameBall.Acceleration.Y = gravity;
                GameBall.Acceleration.X = 0;
                
                if (GameBall.overPaddle(Player, BoardHeight) && GameBall.bottom(BoardHeight))
                {
                    GameBall.hitPaddle();
                    bounceCount++;
                }
                else if(GameBall.hitWall(BoardWidth))
                {
                    GameBall.bounceX(BoardWidth);
                }
                else if (GameBall.top())
                {
                    GameBall.Position.Y = BoardHeight-(GameBall.Radius+40);
                    elevation++;
                }
                else if(GameBall.bottom(BoardHeight))
                {   
                    if (elevation > 0)
                    {
                        GameBall.Position.Y = 5;
                        elevation--;
                    }
                    else
                    {
                        MessageBox.Show("game over");
                        this.IsActive = false;
                        //play epic failure music
                        //figure out how to close without leaving Bounce.exe daemon
                    }
                }

                if (MouseState == 1 && GameBall.overPaddle(Player, BoardHeight))
                {
                    GameBall.Acceleration.Y = -30;
                    //GameBall.Acceleration.X = -10;
                }
                else if (MouseState == 2 && GameBall.overPaddle(Player, BoardHeight))
                {
                    GameBall.Acceleration.Y = 100;
                    //GameBall.Acceleration.X = 10;
                }

                GameBall.AdvanceTime(timeStep);
            }
        }
    }
}
