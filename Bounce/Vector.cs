﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bounce
{
    class Vector
    {
        public Vector(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }


        public Vector()
        {
            this.X = 0;
            this.Y = 0;
        }

        public float X { get; set; }
        public float Y { get; set; }
    }
}
